package intentcamera.appcamera;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private EditText inputNome;
    private EditText inputSobrenome;
    private Button buttonCancel;
    private Button buttonFoto;

    private TextView inputDataNasc;
    private DatePickerDialog.OnDateSetListener datePickerListener;
    static final String TAG = "MainActivity";
    private String data;

    private String nome;
    private String sobrenome;

    private String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 1;
    private Bitmap imageBitmap;

    private Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputNome = (EditText) findViewById(R.id.etNome);
        inputSobrenome = (EditText) findViewById(R.id.etSobrenome);
        inputDataNasc = (TextView) findViewById(R.id.tvDataNasc);
        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonFoto = (Button) findViewById(R.id.buttonFoto);

        datePickerListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int dia, int mes, int ano) {
                mes = mes +1;
                Log.d(TAG, "onDateSet: dd/mm/yyyy "+ dia + "/" + mes + "/" + ano );
                data = ano + "/" + mes + "/" + dia;
                inputDataNasc.setText(data);
            }
        };

    }

    public void mostrarDatePickerDialog(View v){

        Calendar calendar = Calendar.getInstance();
        int ano = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        int dia = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dateDialog = new DatePickerDialog(MainActivity.this,
                android.R.style.ThemeOverlay_Material_Dialog_Alert, datePickerListener, dia, mes, ano);

        dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dateDialog.show();

    }

    public void dispatchTakePictureIntent(View v) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Bundle extra = data.getExtras();
            imageBitmap = (Bitmap) extra.get("data");

            mostrarResultado();
        }
    }

    public void mostrarResultado(){

        nome = String.valueOf(inputNome.getText());
        sobrenome = String.valueOf(inputSobrenome.getText());

        Intent resultado = new Intent(this, ResultActivity.class);
        bundle = new Bundle();

        bundle.putString("nome", nome);
        bundle.putString("sobrenome", sobrenome);
        bundle.putString("dataNasc", data);
        bundle.putParcelable("foto", imageBitmap);

        resultado.putExtras(bundle);

        startActivity(resultado);

    }

    public void limpar(View v){
        inputNome.setText("");
        inputSobrenome.setText("");
        inputDataNasc.setText("");
    }


}


