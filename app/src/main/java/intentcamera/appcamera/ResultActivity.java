package intentcamera.appcamera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Juliana on 09/08/2017.
 */

public class ResultActivity extends AppCompatActivity{

    TextView tvNome;
    TextView tvSobrenome;
    TextView tvDataNasc;
    ImageView fotoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_resultado);

        tvNome = (TextView) findViewById(R.id.tvNome);
        tvSobrenome = (TextView) findViewById(R.id.tvSobrenome);
        tvDataNasc = (TextView) findViewById(R.id.tvData);
        fotoView = (ImageView) findViewById(R.id.fotoView);

        Intent resultado = getIntent();

        Bundle bundle = resultado.getExtras();

        String nome = bundle.getString("nome");
        String sobrenome = bundle.getString("sobrenome");
        String dataNasc = bundle.getString("dataNasc");
        Bitmap foto = bundle.getParcelable("foto");

        tvNome.setText(nome);
        tvSobrenome.setText(sobrenome);
        tvDataNasc.setText(dataNasc);
        fotoView.setImageBitmap(foto);

    }
}
